function canvasTextBox(KWARGS) {

	/*HANDLE ERRORS*/
	if (arguments.length != 1) {
		console.log("You must pass this object");
		console.log("{context:required, text:optional, x:optional, y:optional, width:optional, font:optional, trimLine:optional}");
		throw "Must pass an object of key/value pairs as the only argument";
	} else if (!KWARGS instanceof Object) {
		console.log("You must pass this object");
		console.log("{context:required, text:optional, x:optional, y:optional, width:optional, font:optional}, trimLine:optional");
		throw "Argument must be an object of key/value pairs";
	}
	/**/

	//Loading in this argument
	var ctx = KWARGS.context;

	/*CHECK ctx IS CORRECT TYPE*/
	if (ctx==undefined) {
		throw "Keyword Argument 'context' must be defined";
	} else if (!(ctx instanceof CanvasRenderingContext2D)) {
		throw "'context' must be a canvas context object";
	}
	/**/

	//Loading in the arguments
	var text = (KWARGS.text || "").toString();
	var width = KWARGS.width == undefined ? 0 : KWARGS.width;
	var font = KWARGS.font || ctx.font;

	//Reusable function
	var outputLine = function() {
		lines.push(line);
		line = "";
		lineStarted = false;
	}

	//Variables related to lines
	var lines = [];
	var line = "";
	var lineStarted = false;
	var lineHeight = parseInt( font.match(/(\d+)px/)[1] );

	//Split the text into an array of words, empty space, and single newline characters
	// ["WordA", " ","WordB", "\n", "     ", "WordC", "   ", "WordD", "   ", "\n", "WordE"]
	words = text.split(/(\n|[^\S\n]+)/m);
	for (var i=words.length-1;i>=0;--i) {
		if (words[i].length==0) {
			words.splice(i,1);
		}
	}

	var breakWord; //Variable that keeps track of whether the word is being broken across multiple lines

	//Some defined constants
	var WORD_WHITESPACE = 0;
	var WORD_NEWLINE = 1;
	var WORD_TEXT = 2;

	//This will be set to one of the above constants
	var wordType;

	//Save the canvas state before modifying it
	ctx.save();
	ctx.font = font;

	var numberOfWords = words.length;
	for (var i=0;i<numberOfWords;i++) {

		var getWord = words[i];

		//Get the wordType
		if (/\n/.test(getWord)) wordType = WORD_NEWLINE;
		else if (/\s+/.test(getWord)) wordType = WORD_WHITESPACE;
		else wordType = WORD_TEXT;


		if (wordType==WORD_NEWLINE) {
			outputLine();
			//line = "\n";
			lineStarted = true;
			continue;
		}

		lineStarted = true;


		var wordWidth = ctx.measureText(getWord).width;

		//Only break the word if it is actual text, and it is too long to fit on one line even on its own
		if (wordType==WORD_TEXT && wordWidth > width) {
			breakWord = true;
		} else {
			breakWord = false;
		}
		//If the line is not empty, then end the line and start the broken word on a newline.
		if (line != "" && breakWord) {
			outputLine();
		}

		if (breakWord) {
			//Loop through the word's letters
			for (var c in getWord) {
				var letter = getWord[c];
				var nextWidth = ctx.measureText(line+letter).width;
				if (nextWidth >= width) {
					//If we have reached the end of the line, then output it,
					//set the current word to what's left to be processed
					//and then subtract 1 from i, so on the next iteration we
					//are still on this word
					words[i] = getWord.slice(c);
					--i;
					outputLine();
					break;
				} else {
					line += letter; //Otherwise, if there's space, add the letter to the line
				}
			}
		}

		//If the line does not need to be broken
		//Then we need to figure out if it can fit on the current line,
		//or if it needs to go on the next line
		else {
			var nextWidth = ctx.measureText(line+getWord).width;
			//If word is too long for current line
			if (nextWidth > width) {
				if (wordType==WORD_TEXT) {
					--i; //If text, repeat this word next iteration if the word won't fit
				} else {
					line += getWord; //If word is invisible, add to current line
				}
				outputLine(); //Output the line as we have reached its limit
			} else {
				line += getWord; //If the word can fit, then add it to the line!
			}
		}
	}

	//Output any leftovers
	if (lineStarted) {
		outputLine();
	}

	ctx.restore(); //Restore the canvas to its previous state

	//Return TextBox object
	return {
		context: ctx,
		lines: lines,
		width: width,
		height: lines.length*lineHeight,
		fontSize: lineHeight,
		font: font
	};
}

function renderTextBox(textBox,KWARGS,callback) {
	var ctx = textBox.context;
	ctx.save();
	ctx.textBaseline = "top";
	ctx.font = textBox.font;
	var fontSize = textBox.fontSize;
	var width = textBox.width;
	var border = KWARGS.border ? true : false;
	var x = KWARGS.x == undefined ? 0 : KWARGS.x;
	var y = KWARGS.y == undefined ? 0 : KWARGS.y;
	var startY = y;
	var lines = textBox.lines;
	for (var i in lines) {
		var getLine = lines[i].replace(/\n/g,"");
		if (!callback) {
			ctx.fillText(getLine,x,y);
		} else {
			callback(ctx,getLine,x,y);
		}
		y += fontSize;
	}
	if (border) {
		ctx.strokeRect(x,startY,width,fontSize*lines.length);
	}
	ctx.restore();
}