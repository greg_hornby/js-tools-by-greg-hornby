function TupleHash() {
	this.columns = {};
	this.tupleSize = 0;
	this.columnHashes = [];
	this.storage = {};
	this.storageIndex = 0;
	if (arguments.length==1 && typeof arguments[0] == "number") {
		var tupleSize = arguments[0];
		this.tupleSize = tupleSize;
		for (var i=0;i<tupleSize;i++) {
			this.columns[i] = i;
			this.columnHashes[i] = {};
		}
	} else {
		this.tupleSize = arguments.length;
		for (var i in arguments) {
			this.columns[arguments[i]] = i;
			this.columnHashes[i] = {};
		}
	}
}

TupleHash.prototype.set = function(tuple,value) {
	if (tuple.length != this.tupleSize) {
		throw "Tuple must be of length " + this.tupleSize;
	}
	var tupleId = null;
	for (var i in tuple) {
		var key = tuple[i];
		var hash = this.columnHashes[i];
		if ((key in hash)) {
			tupleId = hash[key];
			var expireTuple = this.storage[tupleId][0];
			for (var j in expireTuple) {
				var expireKey = expireTuple[j];
				this.columnHashes[j][expireKey] = undefined;
			}
			this.storage[tupleId] = undefined;
		}
	}
	tupleId = (tupleId==null ? this.storageIndex++ : tupleId);
	this.storage[tupleId] = [tuple.slice(),value];
	for (i in tuple) {
		var key = tuple[i];
		var hash = this.columnHashes[i];
		hash[key] = tupleId;
	}
}

TupleHash.prototype.get = function(column,key) {
	var hash = this.columnHashes[this.columns[column]];
	if (key in hash) {
		var tupleId = hash[key];
		var tupleObj = this.storage[tupleId];
		var returnObj = {keys:{},keysArray:[],value:null};
		for (var key in this.columns) {
			var index = this.columns[key];
			returnObj.keys[key] = tupleObj[0][index];
			returnObj.keysArray[index] = tupleObj[0][index];
		}
		returnObj.value = tupleObj[1];

		return returnObj;
	} else {
		return undefined;
	}
}

try {
	module.exports = TupleHash;
} catch(e) {

}